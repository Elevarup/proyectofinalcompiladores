OBJS = obj/inicio.o obj/salida.o obj/principal.o obj/imageloader.o
BINARY = bin/ejecutable
LIBS = -lGL -lGLU -lm -lglut
HEADER = src/header/funciones.h src/header/imageloader.h


all: $(BINARY)

obj/%.o: src/%.cpp $(HEADER)
	g++ -c $<; mv $(@F) obj

$(BINARY): $(OBJS) $(LIBS)
	g++ -o $(BINARY) $(OBJS) $(LIBS)

clean:
	rm -rf obj/* bin/*
