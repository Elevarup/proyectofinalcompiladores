#ifndef _FUNCIONES_H_
#define _FUNCIONES_H_

#include <iostream>
#include <GL/glut.h>
#include <math.h>
#include "imageloader.h"
using namespace std;

//inicio
void inicializar(double);
void dibujar();
void tecladoSimple(unsigned char,int,int);

//salida
void grilla();


#endif
