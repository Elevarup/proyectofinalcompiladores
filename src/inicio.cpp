#include "header/funciones.h"

void inicializar(double cuadrado){
	glClearColor(1.0,1.0,1.0,1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60,1,1,1000);
	//glOrtho(-cuadrado,cuadrado,-cuadrado,cuadrado,-cuadrado,cuadrado);
	glMatrixMode(GL_MODELVIEW);
}

void dibujar(){
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0,0.0,0.0);
	glLoadIdentity();
	gluLookAt(10,9,10,0,0,0,0,1,0);
	glPushMatrix();

	//glRotatef(5,1.0,1.0,0.0);	
	grilla();
	glColor3f(1.0,0.0,0.0);
		glutWireTeapot(1);
	glPopMatrix();
	glutSwapBuffers();
}

void tecladoSimple(unsigned char key,int x, int y){
	switch(key){
		case 27:exit(1);
	}
	glutPostRedisplay();
}
