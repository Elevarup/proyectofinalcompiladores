#include "header/funciones.h"

int main(int argc,char** argv){
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(800,800);
	glutInitWindowSize(1000,1000);
	glutCreateWindow("PROYECTO FINAL");

	inicializar(15);
	glutDisplayFunc(dibujar);
	glutKeyboardFunc(tecladoSimple);

	glEnable(GL_DEPTH_TEST);
	glutMainLoop();
	
	return 0;
}
	
